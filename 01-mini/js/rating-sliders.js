$(function () {

  //Goes through the table and "re-arranges things"
  setupRatings("table.criteria tbody");

});

var setupRatings = function (selector) {

  // Upper limit to determine where labels below are removed
  var MAX_OPTS_BEFORE_NOLABELS = 5;
  // Upper limit where no value underneath (for big ranges)
  var MAX_OPTS_BEFORE_NOTICKS = 25;

  $("tr", selector).each(function (i, e) {
    var $tr = $(e);
    var $slide = $(".slider", $tr);
    var $score = $tr.find("input[name='rawScoreList']");
    var min = parseFloat($slide.data('min-value'));
    var max = parseFloat($slide.data('max-value'));
    var tickLabels = {};

    tickLabels[min] = $(".left-label", $tr).text();
    tickLabels[max] = $(".right-label", $tr).text();

    if (min === 0 && max === 1) {

      // This is a binary choice

      // Create the new control
      var $html = $("<div class='binary'>" +
        "<a href='#' class='button secondary radius' data-val='" + min + "'>" + tickLabels[min] + "</a>" +
        " Select An Option " +
        "<a href='#' class='button secondary radius' data-val='" + max + "'>" + tickLabels[max] + "</a></div>");

      // Add it in place of the original slider
      $(".slider", $tr).html($html);

      // Delegate choice click to the row
      $("a.button", $tr).on('click', function (e) {
        var $set = $(this).parents('.binary');
        $("a.button").addClass('secondary');
        $(this).removeClass('secondary');
        $score.val($(this).data('val'));
        $tr.removeClass('un-rated');
        e.preventDefault();
      });

    } else {
      // Should use a slider
      var cfg = {
        min: min,
        max: max,
        step: parseFloat($slide.data('inc-value')),

        slide: function (event, ui) {
          $score.val(ui.value);
        },
        stop: function (event, ui) {
          $score.val(ui.value);
          $tr.removeClass('un-rated');
        }
      };

      var range = (max - min);
      if (range < MAX_OPTS_BEFORE_NOLABELS) {
        cfg.tickLabels = tickLabels;
        $(".slider", $tr).labeledslider(cfg);
      }
      if (range < MAX_OPTS_BEFORE_NOTICKS) {
        $(".slider", $tr).labeledslider(cfg);
      } else {
        $(".slider", $tr).slider(cfg);
      }

      if (range < MAX_OPTS_BEFORE_NOTICKS) {
        //Remove these labels for the new slider
        $(".left-label,.right-label", $tr).remove();
      }

    }



    // Set row as "un touched"
    if (parseFloat($score.val()) === -1) {
      $score.val("No Rating");
      $tr.addClass('un-rated');
    }


  });

};