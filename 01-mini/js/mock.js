$(function () {
  //Mocking Setup
  //Little bit to fix jquery-ui datepicker offset and foundation (not needed outside mock)
  $.extend($.datepicker, {
    _checkOffset: function (inst, offset, isFixed) {
      return $(this._lastInput).offset();
    }
  });

  $("#save-btn").on('click', function (e) {
    e.preventDefault();
    if ($("tr.un-rated").length > 0) {
      if (!confirm("There are un-scored items. Close without scoring?", "Yes", "No")) {
        return;
      }
    }
    $("#mock").html("<h1>Saved!</h1>");
  });

  // End Mocking

});