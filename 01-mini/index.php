<? include('header.php'); ?>

<h1 class='text-center'>Skild Controls</h1>
<hr>


<div class="row">
  <div class="columns"><h2>Time Picking</h2>
  <hr></div>
</div>

<!-- Start date / time pairs -->
<div class="timeRow">
<div class="row">
  <div class="columns"><h5 class='subheader'>Phase 1 <span class='label secondary'></span></h5></div>
</div>
<div class="row">
<div class="columns small-3">
  <input type="text" class="date start" placeholder="Start Date" />
</div>
<div class="columns small-3">
  <input type="text" class="time start" placeholder="Start Time"/>
</div>
<div class="columns small-3">
   <input type="text" class="time end" placeholder="End Time"/>
</div>
<div class="columns small-3">
  <input type="text" class="date end" placeholder="End Date"/>
</div>
</div>
</div>

<div class="timeRow">
<div class="row">
  <div class="columns"><h5 class='subheader'>Phase 2 <span class='label secondary'></span></h5></div>
</div>
<div class="row">
<div class="columns small-3">
  <input type="text" class="date start" placeholder="Start Date" />
</div>
<div class="columns small-3">
  <input type="text" class="time start" placeholder="Start Time"/>
</div>
<div class="columns small-3">
   <input type="text" class="time end" placeholder="End Time"/>
</div>
<div class="columns small-3">
  <input type="text" class="date end" placeholder="End Date"/>
</div>
</div>
</div>

<!-- end date/time pairs -->


<div class="row">
  <div class="columns">
  <hr>
  <h2>Ratings</h2>
  <hr>
  </div>
</div>

<div class="row">
  <div class="columns">
    <a href="#" id="save-btn" class="button success right tiny radius">Save And Close</a>
  </div>
</div>

<div class="row">
<div id="mock" class='columns'>
<!-- this just loads data copied from current admin -->
<? include("mock/sample_01.php"); ?>
</div>
</div>

<!-- this is only needed for this mockup -->
<script src='js/mock.js'></script>
<!-- this is required for each module -->
<script src='js/date-time-pairs.js'></script>
<script src='js/rating-sliders.js'></script>

<? include('footer.php'); ?>